import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native'
import {
  showNotification, 
  handleCancel,
  handleScheduleNotification
} from './src/notification.android'
import OneSignal from 'react-native-onesignal'

export default class App extends Component {
  componentDidMount(){
    OneSignal.requestPermissions({
      alert: true,
      badge: true,
      sound: true,
    })
    OneSignal.init('92e6a6ee-e77d-4664-a937-ff6d98f9c478')
    OneSignal.addEventListener('received', (data) => {
      console.log(data)
    });
    // OneSignal.inFocusDisplaying(2);

  }

//   onOpened = (openResult) => {
//     Linking.openURL
// (openResult.notification.payload.additionalData[1]);
//   };

  componentWillUnmount() {
    OneSignal.removeEventListener('received');
  }

  render() {
    return (
      <View style={styles.container}>
        <Text> Hello + Hello + Hello + Hello </Text>
        <TouchableOpacity
          style={styles.button}
          onPress={() => showNotification('Hello', 'testing')}
        >
          <Text>Click Me to Get Notification</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.button}
          onPress={() => handleScheduleNotification('HI', 'Notifi after 5 sec')}
        >
          <Text>Click Me to Get Notifi after 5 sec</Text>
        </TouchableOpacity>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  button: {
    width: 200,
    height: 70,
    borderRadius: 35,
    backgroundColor: 'lightblue',
    alignItems: 'center',
    justifyContent: 'center'
  }
})